import 'package:app/src/layout/login/LoginBloc.dart';
import 'package:app/src/layout/login/componets/AnimatedButtons.dart';
import 'package:app/src/layout/login/componets/AnimatedLogo.dart';
import 'package:bloc_pattern/bloc_pattern.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

class LoginWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle.light);
    return BlocProvider<LoginBloc>(
        bloc: LoginBloc(context), child: _LoginContainer());
  }
}

class _LoginContainer extends StatefulWidget {
  @override
  __LoginContainerState createState() => __LoginContainerState();
}

class __LoginContainerState extends State<_LoginContainer>
    with TickerProviderStateMixin {
  @override
  Widget build(BuildContext context) {
    var bloc = BlocProvider.of<LoginBloc>(context);
    var size = MediaQuery.of(context).size;

    return Scaffold(
      body: SingleChildScrollView(
        controller: bloc.scrollController,
              child: Container(
          height: size.height,
          width: size.width,
          child: Stack(
            children: <Widget>[
              Container(
                  height: double.infinity,
                  width: double.infinity,
                  decoration: BoxDecoration(
                    image: DecorationImage(
                      image: ExactAssetImage('assets/imgs/background.jpg'),
                      fit: BoxFit.cover,
                    ),
                  )),
              Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  AnimatedLogo(completed: (result){
                    if (result == "loading")
                      bloc.loginVerify();
                  },),
                  StreamBuilder(
                      stream: bloc.initOut,
                      initialData: 0.0,
                      builder: (context, AsyncSnapshot<double> snapshot) =>
                          AnimatedSize(
                            curve: Curves.easeInOut,
                            vsync: this,
                            duration: Duration(milliseconds: 800),
                            child: Container(
                              height: snapshot.data,
                            ),
                          )),
                  StreamBuilder(
                      stream: bloc.initOut,
                      initialData: 0.0,
                      builder: (context, AsyncSnapshot<double> snapshot) =>
                          AnimatedOpacity(
                            duration: Duration(milliseconds: 300),
                            opacity: snapshot.data == 0.0 ? 0.0 : 1.0,
                            child: AnimatedButtons(),
                          )),
                ],
              ),
            ],
          ),
        ),
      ),
    );
  }
}
