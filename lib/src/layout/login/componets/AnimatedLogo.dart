import 'package:app/src/layout/login/LoginBloc.dart';
import 'package:bloc_pattern/bloc_pattern.dart';
import 'package:flare_flutter/flare_actor.dart';
import 'package:flutter/material.dart';

class AnimatedLogo extends StatefulWidget {
  final String animation;
  final double height;
  final double width;
  final Function(String) completed;

  const AnimatedLogo(
      {Key key,
      this.animation = "loading",
      this.height = 150,
      this.width = 300,
      this.completed})
      : super(key: key);

  @override
  _AnimatedLogoState createState() => _AnimatedLogoState();
}

class _AnimatedLogoState extends State<AnimatedLogo> {
  String animation;
  @override
  void initState() {
    super.initState();
    animation = widget.animation;
  }

  @override
  Widget build(BuildContext context) {
    return Container(
        height: widget.height,
        width: widget.width,
        child: FlareActor("assets/flare/Lagoinha.flr",
            alignment: Alignment.center,
            fit: BoxFit.contain,
            animation: animation, callback: (result) {
          if (widget.completed != null) widget.completed(result);
          if (result == "loading") {
            setState(() {
              animation = "voar";
            });
          }
        }));
  }
}
