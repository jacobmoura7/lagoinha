import 'dart:io';

import 'package:app/src/layout/home/HomeWidget.dart';
import 'package:app/src/layout/inicio/InicioWidget.dart';
import 'package:app/src/models/UsuarioModel.dart';
import 'package:app/src/services/ApiService.dart';
import 'package:bloc_pattern/bloc_pattern.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:flutter_masked_text/flutter_masked_text.dart';
import 'package:rxdart/rxdart.dart';
import 'package:image_picker/image_picker.dart';
import 'package:image_cropper/image_cropper.dart';

class PerfilBloc extends BlocBase {
  BuildContext context;
  PerfilBloc(this.context);

  final nameController = TextEditingController();

  final _userImageController = BehaviorSubject<String>();
  Observable<String> get userImageOut => _userImageController.stream;

  final _estadoCivilController =
      BehaviorSubject<int>(seedValue: UsuarioModel().perfil.estadocivil);
  Sink<int> get estadoCivilIn => _estadoCivilController.sink;
  Observable<int> get estadoCivilOut => _estadoCivilController.stream;

  final _sexoController = BehaviorSubject<String>(seedValue: UsuarioModel().perfil.sexo);
  Sink<String> get sexoIn => _sexoController.sink;
  Observable<String> get sexoOut => _sexoController.stream;

  final autoCompleteController =
      TextEditingController(text: UsuarioModel().perfil.igrejanome);
  final aniversarioController = MaskedTextController(
      mask: "00/00/0000", text: UsuarioModel().perfil.datanascimento);

  final _loadingController = BehaviorSubject<bool>(seedValue: false);
  Sink<bool> get loadingIn => _loadingController.sink;
  Observable<bool> get loadingOut => _loadingController.stream;

  int _gcd(int width, int height) {
    return (height == 0) ? width : _gcd(height, width % height);
  }

  save() async {
    _loadingController.add(true);
    var user = await FirebaseAuth.instance.currentUser();
    UsuarioModel().perfil.nome = nameController.text;
    UsuarioModel().perfil.datanascimento = aniversarioController.text;
    UsuarioModel().perfil.sexo = _sexoController.value;
    UsuarioModel().perfil.estadocivil = _estadoCivilController.value;
    UserUpdateInfo info = UserUpdateInfo();
    info.displayName = UsuarioModel().perfil.nome;
    await user.updateProfile(info);
    await user.reload();
    bool isSaved = await apiService.savePerfil(user.uid);
    await UsuarioModel().save();
    // if (isSaved) {
    //   UsuarioModel().save();
    //   Navigator.pushReplacement(context, FadePageRoute(widget: HomeWidget()));
    // } else {
    //   _snack("Erro ao salvar dados");
    //   _loadingController.add(false);
    // }
  }

  pickerImage(bool camera) async {
    int width = 200;
    int height = 200;

    var imagePhoto = await ImagePicker.pickImage(
        source: camera ? ImageSource.camera : ImageSource.gallery);
    File image = await ImageCropper.cropImage(
      toolbarTitle: 'Editar',
      sourcePath: imagePhoto.path,
      ratioX: width / _gcd(width, height),
      ratioY: height / _gcd(width, height),
      maxWidth: width,
      maxHeight: height,
    );
    var user = await FirebaseAuth.instance.currentUser();
    print(user.uid);
    String url = await apiService.uploadImage(image, user.uid);
    print(url);
    if (url != null) {
      print("UPLOAD COMPLETED...");
      UsuarioModel().perfil.photo = url;
      var info = UserUpdateInfo();
      info.photoUrl = url;
      await user.updateProfile(info);
      await user.reload();
      _userImageController.add(url);
    } else {
      _snack("Erro no upload de imagem");
    }
  }

  _snack(String text) {
    final snackBar = SnackBar(content: Text(text));
    Scaffold.of(context).showSnackBar(snackBar);
  }

  @override
  void dispose() {
    _estadoCivilController.close();
    _sexoController.close();
    _loadingController.close();
    _userImageController.close();
  }
}

class FadePageRoute extends PageRouteBuilder {
  final Widget widget;

  FadePageRoute({this.widget})
      : super(pageBuilder: (BuildContext context, Animation<double> animation,
            Animation<double> secondaryAnimation) {
          return widget;
        }, transitionsBuilder: (BuildContext context,
            Animation<double> animation,
            Animation<double> secondaryAnimation,
            Widget child) {
          return new FadeTransition(
            opacity: animation,
            child: child,
          );
        });
}


