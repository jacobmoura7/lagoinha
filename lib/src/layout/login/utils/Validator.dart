import 'dart:async';

class Validator {
  final numberValidator = StreamTransformer<String, String>.fromHandlers(
      handleData: (number, sink) {
    if (number.isEmpty) {
      sink.addError("O campo não pode ser vazio");
    } else {
      sink.add(number);
    }
  });

}
