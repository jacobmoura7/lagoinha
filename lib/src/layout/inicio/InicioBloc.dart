import 'package:app/src/models/UsuarioModel.dart';
import 'package:bloc_pattern/bloc_pattern.dart';
import 'package:flutter/material.dart';
import 'package:rxdart/rxdart.dart';

class InicioBloc implements BlocBase {

  InicioBloc();


  final _postsController = BehaviorSubject<List<Post>>(seedValue: UsuarioModel().home[0].posts);
  Sink<List<Post>> get postsIn => _postsController.sink;
  Observable<List<Post>> get postsOut => _postsController.stream;
  
  
  
  @override
  void dispose(){
      _postsController.close();
  }
}