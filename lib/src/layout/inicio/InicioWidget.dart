import 'package:app/src/layout/inicio/InicioBloc.dart';
import 'package:app/src/models/UsuarioModel.dart';
import 'package:app/src/utils/icons_helper.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

class InicioWidget extends StatefulWidget {
  @override
  _InicioContainerState createState() => _InicioContainerState();
}

class _InicioContainerState extends State<InicioWidget> {
  InicioBloc bloc;

  Widget _buttonCategorie(Home c) {
    return Padding(
      padding: const EdgeInsets.all(5.0),
      child: RaisedButton.icon(
        color: Colors.purple,
        textColor: Colors.white,
        onPressed: () {
          print(c.icon);
          bloc.postsIn.add(c.posts);
        },
        label: Text(c.tabname),
        icon: Icon(getIconGuessFavorFA(name: c.icon)),
      ),
    );
  }

  Widget _listItemCategories() {
    return ListView(
        scrollDirection: Axis.horizontal,
        children: UsuarioModel().home.map((c) => _buttonCategorie(c)).toList());
  }

  Widget _card(Post post) {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: Material(
        color: Colors.white,
        elevation: 7,
        borderRadius: BorderRadius.circular(7),
        child: Column(
          children: <Widget>[
            Container(
              width: double.infinity,
              height: 180,
              child: ClipRRect(
                borderRadius: BorderRadius.only(
                  topLeft: Radius.circular(7),
                  topRight: Radius.circular(7),
                ),
                child: CachedNetworkImage(
                  fit: BoxFit.cover,
                  imageUrl: post.imagemLink == null
                      ? "http://www.almguide.com/wp-content/themes/K-Theme/assets/images/No-image-full.jpg"
                      : post.imagemLink,
                  placeholder: Padding(
                      padding: EdgeInsets.symmetric(
                          horizontal: MediaQuery.of(context).size.width * 0.15),
                      child: CircularProgressIndicator()),
                  errorWidget: new Icon(Icons.error),
                ),
              ),
            ),
            Container(
                padding: EdgeInsets.all(8),
                child: Text(
                  post.titulo,
                  textAlign: TextAlign.center,
                  style: TextStyle(fontWeight: FontWeight.bold),
                )),
            Container(
                padding: EdgeInsets.only(bottom: 8, top: 3, left: 12, right: 12),
                width: double.infinity,
                child: Row(
                  children: <Widget>[
                    Icon(FontAwesomeIcons.shareAlt, size: 18, color: Colors.grey,),
                    Expanded(
                      child: Container(),
                    ),
                    Text(
                      post.data,
                      textAlign: TextAlign.right,
                      style: TextStyle(color: Colors.grey),
                    ),
                  ],
                )),
          ],
        ),
      ),
    );
  }

  Widget _listPost() {
    return StreamBuilder(
        stream: bloc.postsOut,
        builder: (context, AsyncSnapshot<List<Post>> snapshot) {
          if (!snapshot.hasData) {
            return Container();
          }

          return ListView.builder(
            padding: EdgeInsets.only(top: 50),
            itemCount: snapshot.data.length,
            itemBuilder: (BuildContext context, int index) {
              return _card(snapshot.data[index]);
            },
          );
        });
  }

  @override
  void initState() {
    bloc = InicioBloc();
    super.initState();
  }

  @override
  void dispose() {
    bloc.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    print(UsuarioModel().home.length);
    return Material(
      child: Stack(
        children: <Widget>[
          _listPost(),
          Container(
              color: Colors.grey[200],
              width: MediaQuery.of(context).size.width,
              height: 40,
              child: _listItemCategories()),
        ],
      ),
    );
  }
}
