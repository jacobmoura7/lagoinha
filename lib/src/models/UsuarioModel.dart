import 'dart:convert';

import 'package:app/src/utils/StorageSave.dart';

class UsuarioModel {
  Token token;
  List<Home> home;
  Perfil perfil;
  final String _keyUser = "USER_MODEL.json";

  factory UsuarioModel.fromJson(Map<String, dynamic> json) {
    var u = UsuarioModel();

    List homeListGeneric = json['home'] as List;
    print(homeListGeneric);
    List<Home> homeList = homeListGeneric.map((i) => Home.fromJson(i)).toList();
    u.home = homeList;

    if (json['token'] != null && json['token'] != "[]")
      u.token = Token.fromJson(json['token']);

    if (json['perfil'] != null && json['perfil'] != "[]")
      u.perfil = Perfil.fromJson(json['perfil']);
    return u;
  }

  Map<String, dynamic> toJson() => {
        'token': token.toJson(),
        'home': home.map((p) => p.toJson()).toList(),
        'perfil': perfil.toJson(),
      };
  // Map<String, dynamic> toJson() => {
  //       'token': jsonEncode(token),
  //       'home': jsonEncode(home),
  //       'perfil': jsonEncode(perfil),
  //     };

  save() async {
    await StorageSave()
        .writeFile(fileName: _keyUser, value: jsonEncode(toJson()));
  }

  Future<bool> load() async {
    try {
      var data =
          json.decode(await StorageSave().readFile(_keyUser));
      UsuarioModel.fromJson(data);
      return true;
    } catch (e) {
      print(e);
      return false;
    }
  }

  factory UsuarioModel() {
    return _singleton;
  }
  static final UsuarioModel _singleton = UsuarioModel._internal();
  UsuarioModel._internal();
}

class Home {
  String tabname;
  String icon;
  String iconcolor;
  List<Post> posts;

  Home({
    this.tabname,
    this.icon,
    this.posts,
    this.iconcolor,
  });

  factory Home.fromJson(Map<String, dynamic> json) {
    var listPosts = json['posts'] as List;
    List<Post> postsList = listPosts.map((i) => Post.fromJson(i)).toList();

    return Home(
      posts: postsList,
      tabname: json['tabname'],
      icon: json['icon'],
      iconcolor: json['iconcolor'],
    );
  }

  Map<String, dynamic> toJson() => {
        'posts': posts.map((p) => p.toJson()).toList(),
        'tabname': tabname,
        'icon': icon,
        'iconcolor': iconcolor,
      };
}

class Post {
  int id;
  int postId;
  String titulo;
  String imagemLink;
  String conteudo;
  String data;

  factory Post.fromJson(Map<String, dynamic> json) {
    return Post(
      id: json['id'],
      postId: json['post_id'],
      titulo: json['titulo'],
      imagemLink: json['imagem_link'],
      conteudo: json['conteudo'],
      data: json['data'],
    );
  }

  Map<String, dynamic> toJson() => {
        'id': id,
        'post_id': postId,
        'titulo': titulo,
        'imagem_link': imagemLink,
        'conteudo': conteudo,
        'data': data,
      };

  Post({
    this.id,
    this.postId,
    this.titulo,
    this.imagemLink,
    this.conteudo,
    this.data,
  });
}

class Perfil {
  int igrejaid;
  int pessoaid;
  String nome;
  String igrejanome;
  String sexo;
  String datanascimento;
  int estadocivil;
  String email;
  String phone;
  String photo;
  String uid;
  List<String> acessos;

  factory Perfil.fromJson(Map<String, dynamic> json) {
    return Perfil(
      igrejaid: json['igrejaid'],
      pessoaid: json['pessoaid'],
      sexo: json['sexo'],
      datanascimento: json['datanascimento'],
      estadocivil: json['estadocivil'] is String ? 1 : json['estadocivil'],
      email: json['email'],
      phone: json['phone'],
      photo: json['photo'],
      uid: json['uid'],
      igrejanome: json['igrejanome'],
      acessos: List<String>.from(json['acessos']),
    );
  }

  Map<String, dynamic> toJson() => {
        'igrejaid': igrejaid,
        'pessoaid': pessoaid,
        'sexo': sexo,
        'datanascimento': datanascimento,
        'estadocivil': estadocivil,
        'email': email,
        'phone': phone,
        'uid': uid,
        'acessos': acessos,
      };

  Perfil({
    this.igrejaid,
    this.pessoaid,
    this.nome,
    this.sexo,
    this.datanascimento,
    this.estadocivil,
    this.email,
    this.phone,
    this.photo,
    this.uid,
    this.acessos,
    this.igrejanome,
  });
}

class Token {
  String tokentype;
  var expiresin;
  String accesstoken;
  String refreshtoken;
  String accessscope;

  factory Token.fromJson(Map<String, dynamic> json) {
    return Token(
      tokentype: json['tokentype'].toString(),
      expiresin: json['expiresin'],
      accesstoken: json['accesstoken'],
      refreshtoken: json['refreshtoken'],
      accessscope: json['accessscope'],
    );
  }

  Map<String, dynamic> toJson() => {
        'tokentype': tokentype,
        'expiresin': expiresin,
        'accesstoken': accesstoken,
        'refreshtoken': refreshtoken,
        'accessscope': accessscope,
      };

  Token({
    this.tokentype,
    this.expiresin,
    this.accesstoken,
    this.refreshtoken,
    this.accessscope,
  });
}
