import 'package:app/src/layout/login/componets/AnimatedLogo.dart';
import 'package:app/src/layout/login/perfil/PerfilBloc.dart';
import 'package:app/src/layout/login/perfil/model/estadocivil.dart';

import 'package:app/src/models/UsuarioModel.dart';
import 'package:app/src/services/ApiService.dart';
import 'package:bloc_pattern/bloc_pattern.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:flutter_typeahead/flutter_typeahead.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

class PerfilWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return BlocProvider<PerfilBloc>(
        bloc: PerfilBloc(context), child: FinalizeContainer());
  }
}

class FinalizeContainer extends StatefulWidget {
  @override
  _FinalizeContainerState createState() => _FinalizeContainerState();
}

class _FinalizeContainerState extends State<FinalizeContainer> {
  @override
  Widget build(BuildContext context) {
    final bloc = BlocProvider.of<PerfilBloc>(context);
    final size = MediaQuery.of(context).size;

    _sexo() {
      return Row(
        children: <Widget>[
          Text(
            "Sexo:",
            style: TextStyle(fontWeight: FontWeight.bold),
          ),
          Container(
            width: 15,
          ),
          StreamBuilder(
              stream: bloc.sexoOut,
              builder: (context, AsyncSnapshot<String> snapshot) {

                if(!snapshot.hasData)
                    return Container();
                return DropdownButton<String>(
                  items: [
                    DropdownMenuItem<String>(
                        value: "M", child: Text("Masculino")),
                    DropdownMenuItem<String>(
                        value: "F", child: Text("Feminino")),
                  ],
                  value: snapshot.data,
                  onChanged: bloc.sexoIn.add,
                );
              }),
        ],
      );
    }

    _estadoCivil() {
      return Row(
        children: <Widget>[
          Text(
            "Estado Civil:",
            style: TextStyle(fontWeight: FontWeight.bold),
          ),
          Container(
            width: 15,
          ),
          StreamBuilder(
              stream: bloc.estadoCivilOut,
              builder: (context, AsyncSnapshot<int> snapshot) {
                if(!snapshot.hasData)
                    return Container();
                List<EstadoCivil> items = EstadoCivil.allEstadoCivil();
                if (items.contains(snapshot.data)) {
                  UsuarioModel().perfil.estadocivil = snapshot.data;
                } else {
                  UsuarioModel().perfil.estadocivil = 1;
                }

                return DropdownButton<int>(
                  items: items.map((value) {
                    return new DropdownMenuItem<int>(
                      value: value.id,
                      child: new Text(value.nome),
                    );
                  }).toList(),
                  value: snapshot.data,
                  onChanged: bloc.estadoCivilIn.add,
                );
              }),
        ],
      );
    }

    _camposOpcionais(){
      return Column(children: <Widget>[
        _sexo(),
                _estadoCivil(),

                //autocomplete

                TextField(
                  controller: bloc.aniversarioController,
                  keyboardType: TextInputType.text,
                  decoration: InputDecoration(
                      border: OutlineInputBorder(),
                      hintText: '12/12/2012',
                      labelText: "Data de Nascimento"),
                ),
      ],);
    }

    return Scaffold(
      body: Stack(
        children: <Widget>[
          Opacity(
            opacity: 0.5,
            child: Container(
              width: double.infinity,
              height: double.infinity,
              child: Image.asset("assets/imgs/back-finalize.png",
                  fit: BoxFit.cover),
            ),
          ),
          Padding(
            padding:
                EdgeInsets.only(left: 12, right: 12, top: size.height * 0.10),
            child: Column(
              children: <Widget>[
                Center(
                  child: Text("MEU PERFIL",
                      style: TextStyle(fontFamily: 'Jambo', fontSize: 24)),
                ),
                Center(
                  child: Container(
                      width: 150,
                      child: Divider(
                        height: 8,
                      )),
                ),
                Center(
                  child: Text("informações de usuário",
                      style: TextStyle(fontSize: 12)),
                ),
                Container(
                  height: 40,
                ),
                HeaderWidget(),
                UsuarioModel().perfil.pessoaid > 0 ? Container(): _camposOpcionais(),
                Container(
                  height: 15,
                ),
                TypeAheadField(
                  textFieldConfiguration: TextFieldConfiguration(
                      controller: bloc.autoCompleteController,
                      autofocus: false,
                      decoration: InputDecoration(
                          border: OutlineInputBorder(),
                          hintText: 'Qual a Lagoinha que você congrega?',
                          labelText: "Sua Lagoinha")),
                  suggestionsCallback: (pattern) async {
                    return await apiService.getIgrejas(pattern);
                  },
                  itemBuilder: (context, suggestion) {
                    return ListTile(
                      leading: Icon(FontAwesomeIcons.church),
                      title: Text(suggestion['nome']),
                    );
                  },
                  onSuggestionSelected: (suggestion) {
                    bloc.autoCompleteController.text = suggestion['nome'];
                    UsuarioModel().perfil.igrejaid = suggestion['id'];
                  },
                ),
              ],
            ),
          ),

          //botao final

          Positioned(bottom: 0.0, child: BotaoFinal())
        ],
      ),
    );
  }
}

class HeaderWidget extends StatelessWidget {
  String _subtitle(FirebaseUser user) {
    if (user.isAnonymous) {
      return "Anônimo";
    } else {
      return user.phoneNumber == null ? user.email : user.phoneNumber;
    }
  }

  @override
  Widget build(BuildContext context) {
    final bloc = BlocProvider.of<PerfilBloc>(context);
    bloc.context = context;
    _showDialog() {
      // flutter defined function
      showDialog(
        context: context,
        builder: (BuildContext context) {
          // return object of type Dialog
          return AlertDialog(
            title: new Text("Trocar imagem"),
            content: new Text(
                "Escolha uma foto na galeria eu tire uma agora mesmo."),
            actions: <Widget>[
              // usually buttons at the bottom of the dialog
              FlatButton(
                child: new Text("Tirar foto"),
                onPressed: () {
                  bloc.pickerImage(true);
                  Navigator.of(context).pop();
                },
              ),
              FlatButton(
                child: new Text("Galeria"),
                onPressed: () {
                  bloc.pickerImage(false);
                  Navigator.of(context).pop();
                },
              ),
            ],
          );
        },
      );
    }

    return Container(
      width: double.infinity,
      height: 100,
      child: StreamBuilder(
        stream: FirebaseAuth.instance.onAuthStateChanged,
        builder: (BuildContext context, AsyncSnapshot<FirebaseUser> snapshot) {
          if (snapshot.data == null) {
            return Container();
          }

          bloc.nameController.text = snapshot.data.displayName == null
              ? "Anônimo"
              : snapshot.data.displayName;
          UsuarioModel().perfil.photo = snapshot.data.photoUrl;
          return Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              GestureDetector(
                onTap: _showDialog,
                child: StreamBuilder(
                    stream: bloc.userImageOut,
                    initialData: snapshot.data.photoUrl,
                    builder: (context, AsyncSnapshot<String> snapimage) {
                      return Container(
                        width: 80,
                        height: 80,
                        child: !snapimage.hasData
                            ? CircleAvatar(
                                child: Text(
                                  bloc.nameController.text[0],
                                  style: TextStyle(fontSize: 18),
                                ),
                              )
                            : CircleAvatar(
                                backgroundImage: NetworkImage(snapimage.data)),
                      );
                    }),
              ),
              Container(
                width: 15,
              ),
              Expanded(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    TextField(
                      controller: bloc.nameController,
                    ),
                    Container(
                      height: 10,
                    ),
                    Text(_subtitle(snapshot.data))
                  ],
                ),
              )
            ],
          );
        },
      ),
    );
  }
}

class BotaoFinal extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final bloc = BlocProvider.of<PerfilBloc>(context);
    final size = MediaQuery.of(context).size;

    return StreamBuilder(
        stream: bloc.loadingOut,
        initialData: true,
        builder: (context, AsyncSnapshot<bool> snapshot) {
          Widget widget;

          if (snapshot.data) {
            widget = AnimatedLogo(
              animation: "loading2",
            );
          } else {
            widget = Text(
              "SALVAR DADOS",
              style: TextStyle(color: Colors.white),
            );
          }

          return Material(
            color: Colors.purple,
            child: InkWell(
              onTap: () {
                if (!snapshot.data) {
                  bloc.save();
                }
              },
              child: AnimatedContainer(
                curve: Curves.easeOut,
                width: size.width,
                height: snapshot.data ? size.height : 67,
                alignment: Alignment.center,
                child: widget,
                duration: Duration(milliseconds: 500),
              ),
            ),
          );
        });
  }
}
