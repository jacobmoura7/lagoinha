
class IgrejaModel {

  final String nome;
  final int id;

  IgrejaModel({this.nome, this.id});

  factory IgrejaModel.fromJson(Map<String, dynamic> json){
    return IgrejaModel(
      nome: json['nome'],
      id: json['id'],
    );
  }
}

