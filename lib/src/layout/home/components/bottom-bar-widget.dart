import 'package:flutter/material.dart';

class BottomBarWidget extends StatefulWidget {
  final Color primaryColor;
  final Color secondaryColor;

  final List<TabItem> items;

  final TabController controller;

  BottomBarWidget(
      {this.primaryColor = Colors.white,
      this.secondaryColor = Colors.orange,
      this.items,
      this.controller});

  @override
  BottomBarWidgetState createState() {
    return new BottomBarWidgetState();
  }
}

class BottomBarWidgetState extends State<BottomBarWidget> {
  String selectedLabel;

  @override
  void initState() {
    super.initState();
    selectedLabel = widget.items[0].label;
    widget.controller.addListener(() {
      setState(() {
        selectedLabel = widget.items[widget.controller.index].label;
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      color: widget.primaryColor,
      child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: widget.items
              .map(
                (d) => _buildItem(
                      icon: d.icon,
                      label: d.label,
                      onTap: () {
                        print(widget.items.indexOf(d));
                        widget.controller.animateTo(widget.items.indexOf(d));
                      },
                    ),
              )
              .toList()),
    );
  }

  Widget _buildItem({IconData icon, String label, Function() onTap}) {
    return (selectedLabel == label)
        ? Expanded(
            child: Container(
              margin: EdgeInsets.symmetric(
                vertical: 10,
                horizontal: MediaQuery.of(context).size.width / 35,
              ),
              padding: EdgeInsets.all(10),
              decoration: BoxDecoration(
                color: widget.secondaryColor,
                borderRadius: BorderRadius.circular(10),
              ),
              child: Icon(icon, color: widget.primaryColor),
            ),
          )
        : Expanded(
            child: GestureDetector(
              onTap: onTap,
              child: Column(
                mainAxisSize: MainAxisSize.min,
                children: <Widget>[
                  Container(
                    padding: EdgeInsets.only(left: 10, right: 10, top: 15),
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(10),
                    ),
                    child: Icon(
                      icon,
                      color: widget.secondaryColor,
                    ),
                  ),
                  Padding(
                    padding: EdgeInsets.only(bottom: 15),
                    child: Text(
                      label,
                      style: TextStyle(
                        color: widget.secondaryColor,
                        fontSize: 13,
                      ),
                    ),
                  ),
                ],
              ),
            ),
          );
  }

  void _showDialog(String text) {
    showDialog(
      context: context,
      barrierDismissible: true,
      builder: (_) => Stack(
            children: <Widget>[
              Positioned(
                width: 200,
                height: 150,
                bottom: (MediaQuery.of(context).size.height - 150) / 2,
                left: (MediaQuery.of(context).size.width - 200) / 2,
                child: Material(
                  child: Center(
                    child: Text(
                      "Você clicou em $text",
                      style: TextStyle(color: Colors.orange, fontSize: 20),
                    ),
                  ),
                ),
              ),
            ],
          ),
    );
  }
}

class TabItem {
  final IconData icon;
  final String label;

  TabItem(this.icon, this.label);
}
