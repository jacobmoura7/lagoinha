import 'package:app/src/layout/login/LoginBloc.dart';
import 'package:app/src/layout/login/componets/TextFieldNumber.dart';
import 'package:app/src/layout/login/componets/TextFieldVerify.dart';
import 'package:app/src/layout/login/utils/AnimatedButtonsTypes.dart';
import 'package:bloc_pattern/bloc_pattern.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

class AnimatedButtons extends StatelessWidget {
  final bool isLoading;
  final Duration duration;

  const AnimatedButtons(
      {Key key,
      this.isLoading = false,
      this.duration = const Duration(milliseconds: 300)})
      : super(key: key);

  _button(IconData iconData, String text, onPressed, Color color) {
    return ButtonTheme(
      height: 50,
      minWidth: 250,
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(15.0)),
      child: RaisedButton.icon(
        textColor: Colors.white,
        color: color,
        onPressed: onPressed,
        icon: Padding(
          padding: const EdgeInsets.only(right: 15, top: 10, bottom: 10),
          child: Icon(iconData),
        ),
        label: Text(text),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    final bloc = BlocProvider.of<LoginBloc>(context);
    bloc.context = context;

    Widget _item1(int value) {
      return AnimatedOpacity(
        duration: duration,
        opacity: value == AnimatedButtonsTypes.SHOW_BUTTONS ? 1.0 : 0.0,
        child: Column(
          children: <Widget>[
            _button(FontAwesomeIcons.phone, "Entrar com telefone", () {
              bloc.buttonsIn.add(AnimatedButtonsTypes.SHOW_FIELD_NUMBER);
            }, Colors.green),
            Container(
              height: 15,
            ),
            _button(FontAwesomeIcons.facebookF, "Entrar com facebook",
                bloc.loginFacebook, Color.fromARGB(255, 59, 89, 152)),
            Container(
              height: 15,
            ),
            _button(FontAwesomeIcons.google, "Entrar com o google",
                bloc.loginGoogle, Colors.red),
            Container(
              height: 30,
            ),
            GestureDetector(
              onTap: bloc.loginAnonymous,
              child: Container(
                child: Text("PULAR", style: TextStyle(color: Colors.white)),
              ),
            ),
          ],
        ),
      );
    }

    Widget _item2(int value) {
      return AnimatedOpacity(
          duration: duration,
          opacity: value == AnimatedButtonsTypes.SHOW_LOADING ? 1.0 : 0.0,
          child: CircularProgressIndicator());
    }

    Widget _item3(int value) {
      return AnimatedOpacity(
          duration: duration,
          opacity: value == AnimatedButtonsTypes.SHOW_FIELD_VERIFY ? 1.0 : 0.0,
          child: TextFieldVerify());
    }

    Widget _item4(int value) {
      return AnimatedOpacity(
          duration: duration,
          opacity: value == AnimatedButtonsTypes.SHOW_FIELD_NUMBER ? 1.0 : 0.0,
          child: TextFieldNumber());
    }

    return StreamBuilder<int>(
        stream: bloc.buttonsOut,
        initialData: 0,
        builder: (context, AsyncSnapshot<int> snapshot) {
          return IndexedStack(
            alignment: Alignment.center,
            index: snapshot.data,
            children: <Widget>[
              _item1(snapshot.data),
              _item2(snapshot.data),
              _item4(snapshot.data),
              _item3(snapshot.data),
            ],
          );
        });
  }
}
