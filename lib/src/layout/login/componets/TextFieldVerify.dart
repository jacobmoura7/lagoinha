import 'dart:async';

import 'package:app/src/layout/login/LoginBloc.dart';
import 'package:bloc_pattern/bloc_pattern.dart';
import 'package:flutter/material.dart';
import 'package:flutter_masked_text/flutter_masked_text.dart';

class TextFieldVerify extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    final bloc = BlocProvider.of<LoginBloc>(context);
    final controllerVerify = MaskedTextController(mask: '000000');

    FocusNode _focus = FocusNode();
    _focus.addListener(() {
      print("Focus");
      print(_focus.hasFocus);
      if (_focus.hasFocus) {
        Timer(Duration(milliseconds: 300), () {
          bloc.scrollController
              .jumpTo(bloc.scrollController.position.maxScrollExtent);
        });
      }
    });

    return Column(
      children: <Widget>[
        Padding(
          padding: const EdgeInsets.only(bottom: 30, left: 60, right: 60),
          child: StreamBuilder(
              stream: bloc.verifyOut,
              initialData: "",
              builder: (context, AsyncSnapshot<String> snapshot) => Theme(
                    data: ThemeData(
                        primaryColor: Colors.green,
                        accentColor: Colors.orange,
                        hintColor: Colors.green),
                    child: TextField(
                        focusNode: _focus,
                        enabled:
                            snapshot.hasData ? snapshot.data.length < 6 : true,
                        scrollPadding: EdgeInsets.all(50),
                        controller: controllerVerify,
                        onChanged: bloc.verifyIn.add,
                        keyboardType: TextInputType.number,
                        style: TextStyle(color: Colors.white),
                        decoration: InputDecoration(
                          border: OutlineInputBorder(),
                          hintStyle: TextStyle(color: Colors.white),
                          labelStyle: TextStyle(color: Colors.white),
                          hintText: '****',
                          labelText: 'Digite o código SMS',
                          errorText: snapshot.error,
                        )),
                  )),
        ),
        StreamBuilder(
            stream: bloc.verifyOut,
            builder: (context, AsyncSnapshot<String> snapshot) {
              if (snapshot.hasData && snapshot.data.length >= 6) {
                bloc.verifyNumber();
                return CircularProgressIndicator();
              } else {
                return Container(
                  height: 30,
                );
              }
            }),
        Container(
          height: 30,
        ),
        OutlineButton(
            textColor: Colors.white,
            child: Text("Cancelar"),
            onPressed: bloc.voltar)
      ],
    );
  }
}
