import 'package:app/src/AppBloc.dart';
import 'package:app/src/layout/login/LoginWidget.dart';
import 'package:bloc_pattern/bloc_pattern.dart';
import 'package:flutter/material.dart';

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {

    return BlocProvider<AppBloc>(
      bloc: AppBloc(context),
      child:MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Lagoinha',
      theme: ThemeData(
        primarySwatch: Colors.purple,
      ),
      home: LoginWidget(),
    )
    );
  }
}