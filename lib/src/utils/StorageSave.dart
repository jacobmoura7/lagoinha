import 'dart:convert';
import 'dart:io';

import 'package:path_provider/path_provider.dart';

// nomes dos arquivos onde estão salvas as coisas
// lojas : "storageStores"
// favoritos : "storageFavorites"
// dashboard : "storageDashboard"
// tab - report_eval_all_tab : "storageReportEvalAll"
// tab - report_eval_month_tab : "storageReportEvalMonth"

// singleton do storage save
// essa classe vai salvar os dados offline
class StorageSave {
  static final StorageSave _storageSave = new StorageSave._internal();

  factory StorageSave() {
    return _storageSave;
  }

  StorageSave._internal();

  // pega o localpath do device
  Future<String> get _localPath async {
    final directory = await getApplicationDocumentsDirectory();
    return directory.path;
  }

  // salva o arquivo no storage do aparelho
  Future<dynamic> saveFile(String fileName) async {
    try {
      // pega o local path
      String path = await _localPath;
      // retorna um arquivo
      File file = new File('$path/$fileName.json');
      // cria o arquivo nessa referência

      // retorna o arquivo
      return file;
    } catch (error) {
      print(error);
    }
  }

// escreve no arquivo
  Future<File> writeFile({String value, String fileName}) async {
    try {
      String path = await _localPath;

      File loadFile = new File('$path/$fileName.json');
      // codifica o valor para json
    //  String parsedValue = jsonEncode(value);
// escreve algo no arquivo
// sobrescreve se já existir algo
      print("ARQUIVO ESCRITO$value ");

      return loadFile.writeAsString(value);

    } catch (error) {
      print(error);
    }
  }

// lê o arquivo
  Future<String> readFile(String fileName) async {
    try {
      // traz o arquivo
      // path do diretório
      final path = await _localPath;

      File loadFile = new File('$path/$fileName.json');

      // Lê o arquivo
      String contents = await loadFile.readAsString();
// retorna a string como json
      print("VALOR LIDO $contents ");
      return contents;
    } catch (error) {
      print(error);
      return null;
    }
  }
}
