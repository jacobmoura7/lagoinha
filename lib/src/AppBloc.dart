import 'package:bloc_pattern/bloc_pattern.dart';
import 'package:flutter/material.dart';
import 'package:firebase_messaging/firebase_messaging.dart';

class AppBloc implements BlocBase {
  final BuildContext context;
  final FirebaseMessaging _firebaseMessaging = FirebaseMessaging();
  AppBloc(this.context) {
    _firebaseMessaging.requestNotificationPermissions();
    _firebaseMessaging.configure(onLaunch: (data) {
      print("FCM - onLaunch");
      print(data);
    }, onMessage: (message) {
      print("FCM - onMessage");
      print(message);
    }, onResume: (data) {
      print("FCM - onResume");
      print(data);
    });

    

  }

  Future<String>  getToken() => _firebaseMessaging.getToken();


  @override
  void dispose() {}
}
