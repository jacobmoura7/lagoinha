import 'dart:io';

import 'package:app/src/AppBloc.dart';
import 'package:app/src/models/UsuarioModel.dart';
import 'package:app/src/utils/Utils.dart';
import 'package:bloc_pattern/bloc_pattern.dart';
import 'package:dio/dio.dart';
import 'package:firebase_auth/firebase_auth.dart';

final ApiService apiService = ApiService();

class ApiService {
  Dio dio;

  ApiService() {
    dio = Dio();
  }

  Dio get getDio => dio;

  getLocation() async {
    try{
        Response response = await dio.get("https://ipapi.co/json/");
        return response.data;
    } on DioError catch (e) {
        return null;
    }
    
  }

  getIgrejas(String params) async {
    Response response = await dio.get('${Utils.URL_API}/igrejas?nome=$params');
    return response.data;
  }

  Future<String> uploadImage(File image, String uid) async {
    FormData formdata = new FormData(); // just like JS
    formdata.add("uid", uid);
    formdata.add("photo", new UploadFileInfo(image, 'photo'));
    try {
      var response = await dio.post<String>('${Utils.URL_API}/uploadphoto',
          data: formdata,
          options: Options(
              method: 'POST',
              responseType: ResponseType.JSON // or ResponseType.JSON
              ));

      return response.data;
    } on DioError catch (e) {
      print(e.message);
      return null;
    }
  }

  Future<bool> savePerfil(String uid) async {
    try {
      FormData formData = new FormData.from({
        "uid": uid,
        "nome": UsuarioModel().perfil.nome,
        "sexo": UsuarioModel().perfil.sexo,
        "datanascimento": UsuarioModel().perfil.datanascimento,
        "estadocivil": UsuarioModel().perfil.estadocivil,
        "email": UsuarioModel().perfil.email,
        "photo": UsuarioModel().perfil.photo,
      });
      await dio.post('${Utils.URL_API}/perfilstart', data: formData);
      print("SALVO!");
      return true;
    } on DioError catch (e) {
      print(e.message);
      print(e.response.data);
      print(e.response.statusCode);
      return false;
    }
  }

  Future<bool> signinWithPhone(FirebaseUser user, {String token = "dsdwefedscds"}) async {
    try {
      FormData formData = new FormData.from({
        "uid": user.uid,
        "phone": user.phoneNumber,
        "mobile": token
      });
      Response response =
          await dio.post('${Utils.URL_API}/signinwithphone', data: formData);
      var model = UsuarioModel.fromJson(response.data);
      await model.save();
      return true;
    } on DioError catch (e) {
      print(e.message);
      return false;
    }
  }

  Future<bool> signinWithEmail(FirebaseUser user, {String token = "dsdwefedscds"}) async {
    try {
      FormData formData = new FormData.from({
        "uid": user.uid,
        "email": user.email,
        "mobile": token
      });
      Response response =
          await dio.post('${Utils.URL_API}/signinwithemail', data: formData);
      var model = UsuarioModel.fromJson(response.data);
      await model.save();
      return true;
    } on DioError catch (e) {
      print(e.message);
      return false;
    }
  }

  Future<bool> signinAnonymously(FirebaseUser user, {String token = "dsdwefedscds"}) async {
    try {

      FormData formData = new FormData.from(
          {"uid": user.uid, "mobile": token});
      Response response =
          await dio.post('${Utils.URL_API}/signinanonymously', data: formData);
      print(response.data);
      var model = UsuarioModel.fromJson(response.data);
      await model.save();
      return true;
    } on DioError catch (e) {
      print(e.message);
      return false;
    }
  }
}
