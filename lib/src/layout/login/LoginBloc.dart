import 'package:app/src/AppBloc.dart';
import 'package:app/src/layout/home/HomeWidget.dart';
import 'package:app/src/layout/login/perfil/PerfilWidget.dart';
import 'package:app/src/layout/login/utils/AnimatedButtonsTypes.dart';
import 'package:app/src/layout/login/utils/Validator.dart';
import 'package:app/src/models/UsuarioModel.dart';
import 'package:app/src/services/ApiService.dart';
import 'package:app/src/services/authentication/Authentication.dart';
import 'package:bloc_pattern/bloc_pattern.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_masked_text/flutter_masked_text.dart';
import 'package:rxdart/rxdart.dart';

class LoginBloc extends BlocBase with Validator {
  BuildContext context;
  MaskedTextController controllerNumber =
      MaskedTextController(mask: '+00 00 0 0000 0000');

  LoginBloc(this.context) {
    _init();
  }

  _init() async {
    var data = await apiService.getLocation();
    if(data != null)
    controllerNumber.text = data['country_calling_code'].toString();
  }

  initLogin() {
    _initController.add(80);
  }

  loginVerify() async {
    FirebaseUser user = await FirebaseAuth.instance.currentUser();
    if (user == null) {
      initLogin();
      return;
    }
    print("passou");
    await UsuarioModel().load();
    String provider = "";
    if (user.providerData.isNotEmpty)
      provider = user.providerData[user.providerData.length - 1].providerId
          .toLowerCase();
    if (user.isAnonymous) {
      await _entrar(user, 3, firstLogin: false);
    } else if (provider == "google.com") {
      await _entrar(user, 2, firstLogin: false);
    } else if (provider == "facebook.com") {
      await _entrar(user, 1, firstLogin: false);
    } else {
      await _entrar(user, 0, firstLogin: false);
    }
  }

  String verificacaoId = "";
  final scrollController = ScrollController();

  final Authentication auth = Authentication();

  final _initController = BehaviorSubject<double>(seedValue: 0);
  Observable<double> get initOut => _initController.stream;

  final _buttonsController =
      BehaviorSubject<int>(seedValue: AnimatedButtonsTypes.SHOW_BUTTONS);
  Sink<int> get buttonsIn => _buttonsController.sink;
  Observable<int> get buttonsOut => _buttonsController.stream;

  final _numberController = BehaviorSubject<String>();
  Sink<String> get numberIn => _numberController.sink;
  Observable<String> get numberOut =>
      _numberController.stream.transform(numberValidator);

  final _verifyController = BehaviorSubject<String>();
  Sink<String> get verifyIn => _verifyController.sink;
  Observable<String> get verifyOut =>
      _verifyController.stream.transform(numberValidator);

  loginNumber() async {
    SystemChannels.textInput.invokeMethod('TextInput.hide');
    if (_numberController.value.length == 18) {
      auth.signWithPhone(_numberController.value, (code) {
        verificacaoId = code;
        buttonsIn.add(AnimatedButtonsTypes.SHOW_FIELD_VERIFY);
      }, (user) {
        _entrar(user, 0);
      }, () {
        _numberController.addError("O número não pode ser verificado");
      });
    } else {
      _numberController.addError("O número informado não é válido");
    }
  }

  voltar() {
    SystemChannels.textInput.invokeMethod('TextInput.hide');
    buttonsIn.add(AnimatedButtonsTypes.SHOW_BUTTONS);
  }

  bool flagPhone = false;

  verifyNumber() async {
    if (flagPhone) {
      return;
    }
    flagPhone = true;
    SystemChannels.textInput.invokeMethod('TextInput.hide');
    var user =
        await auth.verifyPhoneNumber(verificacaoId, _verifyController.value);
    if (user != null) {
      _verifyController.add("");
      await _entrar(user, 0);
      print("executa aqui verifyNumber");
      flagPhone = false;
    } else {
      _verifyController.add("");
      _verifyController.addError("Erro na verificação do código");
    }
  }

  loginGoogle() async {
    buttonsIn.add(AnimatedButtonsTypes.SHOW_LOADING);
    var user = await auth.signWithGoogle();

    if (user != null) {
      await _entrar(user, 2);
    } else {
      buttonsIn.add(AnimatedButtonsTypes.SHOW_BUTTONS);
      _snack("Erro ao efetuar login");
    }
  }

  loginFacebook() async {
    buttonsIn.add(AnimatedButtonsTypes.SHOW_LOADING);
    var user = await auth.signWithFacebook();

    if (user != null) {
      await _entrar(user, 1);
    } else {
      buttonsIn.add(AnimatedButtonsTypes.SHOW_BUTTONS);
      _snack("Erro ao efetuar login");
    }
  }

  loginAnonymous() async {
    buttonsIn.add(AnimatedButtonsTypes.SHOW_LOADING);
    FirebaseUser user = await auth.signinAnonymously();
    print(user.uid);
    if (user != null) {
      await _entrar(user, 3, firstLogin: false);
    } else {
      buttonsIn.add(AnimatedButtonsTypes.SHOW_BUTTONS);
      _snack("Erro ao efetuar login anônimo");
    }
  }

  _resolveUser(FirebaseUser user, int type) async {
    UserUpdateInfo info = UserUpdateInfo();
    String urlImage = _resolverImage(user, type);
    if (urlImage != null) {
      info.photoUrl = urlImage;
    }

    if (user.displayName == null) {
      info.displayName = "Anônimo";
    }
    if (info.displayName != null || info.photoUrl != null) {
      await user.updateProfile(info);
      await user.reload();
    }
  }

  String _resolverImage(FirebaseUser user, int type) {
    if (user.photoUrl == null) {
      return null;
    }
    if (type == 1) {
      return "https://graph.facebook.com/" +
          user.providerData[user.providerData.length - 1].uid +
          "/picture?width=200&height=200";
    } else {
      return null;
    }
  }

  _snack(String text) {
    final snackBar = SnackBar(content: Text(text));
    Scaffold.of(context).showSnackBar(snackBar);
  }

  _entrar(FirebaseUser user, int type, {bool firstLogin = true}) async {
    bool isLoginCompleted = await _apiSendUser(user, type);

    if (!isLoginCompleted) {
      isLoginCompleted = await UsuarioModel().load();
    }

    if (isLoginCompleted) {
      await _resolveUser(user, type);
      buttonsIn.add(AnimatedButtonsTypes.SHOW_BUTTONS);
      Navigator.pushReplacement(
          context,
          MaterialPageRoute(
              builder: (BuildContext context) =>
                  firstLogin ? PerfilWidget() : HomeWidget()));
    } else {
      buttonsIn.add(AnimatedButtonsTypes.SHOW_BUTTONS);
      _snack("Erro ao efetuar login");
    }
  }

  Future<bool> _apiSendUser(FirebaseUser user, int type) async {
    String token = await BlocProvider.of<AppBloc>(context).getToken();
    if (token.isEmpty) {
      token = "kjdsflkdjsfiewfijds";
    }

    if (type == 0) {
      return await apiService.signinWithPhone(user, token: token);
    }
    if (user.isAnonymous) {
      return await apiService.signinAnonymously(user, token: token);
    } else {
      return await apiService.signinWithEmail(user, token: token);
    }
  }

  @override
  void dispose() {
    _verifyController.close();
    _initController.close();
    _buttonsController.close();
    _numberController.close();
  }
}
