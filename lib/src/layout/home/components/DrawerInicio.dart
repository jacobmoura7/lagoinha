import 'package:app/src/layout/login/LoginWidget.dart';
import 'package:app/src/layout/login/perfil/PerfilWidget.dart';
import 'package:app/src/models/UsuarioModel.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:cached_network_image/cached_network_image.dart';


class DrawerHome extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    Perfil perfil = UsuarioModel()?.perfil;
    print(perfil);

    return Drawer(
      child: ListView(
        padding: EdgeInsets.zero,
        physics: ClampingScrollPhysics(),
        children: <Widget>[
          StreamBuilder(
              stream: FirebaseAuth.instance.onAuthStateChanged,
              builder: (context, AsyncSnapshot<FirebaseUser> snapshot) {
                if (!snapshot.hasData) return Container();
                return UserAccountsDrawerHeader(
                  decoration: BoxDecoration(
                    image: DecorationImage(
                        image: AssetImage("assets/imgs/drawer.JPG"),
                        fit: BoxFit.cover,
                        colorFilter: new ColorFilter.mode(
                            Colors.black.withOpacity(0.5), BlendMode.overlay)),
                  ),
                  accountEmail: Text(snapshot.data.email != null
                      ? snapshot.data.email
                      : snapshot.data.phoneNumber),
                  accountName: Text(snapshot.data.displayName),
                  currentAccountPicture: snapshot.data.photoUrl == null ? CircleAvatar(child: Text(snapshot.data.displayName[0].toUpperCase()),) : CircleAvatar(
                    backgroundImage: CachedNetworkImageProvider(snapshot.data.photoUrl),
                  ),
                );
              }),
          ListTile(
            title: Text('Meu Perfil'),
            onTap: () {
              Navigator.pushReplacement(
                  context,
                  MaterialPageRoute(
                      builder: (BuildContext context) => PerfilWidget()));
            },
          ),
          Divider(),
          ListTile(
            title: Text(
              'Sair',
              style: TextStyle(fontWeight: FontWeight.bold),
            ),
            onTap: () async {
              await FirebaseAuth.instance.signOut();
              Navigator.pushReplacement(
                  context,
                  MaterialPageRoute(
                      builder: (BuildContext context) => LoginWidget()));
            },
          ),
        ],
      ),
    );
  }
}
