import 'package:firebase_auth/firebase_auth.dart';
import 'package:google_sign_in/google_sign_in.dart';
import 'dart:async';
import 'package:flutter_facebook_login/flutter_facebook_login.dart';

class Authentication {
  final _firebase = FirebaseAuth.instance;
  final _google = new GoogleSignIn();

  Future<FirebaseUser> signinAnonymously() async {
    return await _firebase.signInAnonymously();
  }
  
  Future<FirebaseUser> signWithGoogle() async {

    final googleAuthentication = await _google.signIn();
    final authenticated = await googleAuthentication.authentication;
    final usuarioAutenticado = await _firebase.signInWithGoogle(
        idToken: authenticated?.idToken,
        accessToken: authenticated?.accessToken);
    print(usuarioAutenticado.email);
    print(usuarioAutenticado.displayName);

    return usuarioAutenticado;
  }
  Future<FirebaseUser> signWithFacebook() async {
    final facebookLogin = new FacebookLogin();
    final FacebookLoginResult result = await facebookLogin.logInWithReadPermissions(['email']);
    final usuarioAutenticado = await _firebase.signInWithFacebook(
      accessToken: result.accessToken.token);
    print(usuarioAutenticado.email);
    print(usuarioAutenticado.displayName);

    return usuarioAutenticado;
  }

  Future<FirebaseUser> verifyPhoneNumber(String verificacaoId, String codigoSms) async {

    final loginResult = await _firebase.signInWithPhoneNumber(
        verificationId: verificacaoId, smsCode: codigoSms);
    return loginResult;

  }

  signWithPhone(String numeroDoCelular, Function(String) code, Function(FirebaseUser) completed, Function error) async {

    return _firebase.verifyPhoneNumber(
        phoneNumber: numeroDoCelular,
        codeSent: (String verified, [int forceResend]) {
            code(verified);
        },
        verificationFailed: (AuthException autenticaoException) {
          print("ocorreu um erro");
          error();
          print(autenticaoException.message);
        },
        verificationCompleted: (FirebaseUser user) {
          completed(user);
        },
        codeAutoRetrievalTimeout: (String timeOut) {
          print(timeOut);
        },
        timeout: Duration(seconds: 30));

  }
}
