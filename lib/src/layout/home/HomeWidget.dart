import 'package:app/src/layout/home/HomeBloc.dart';
import 'package:app/src/layout/home/components/DrawerInicio.dart';
import 'package:app/src/layout/home/components/bottom-bar-widget.dart';
import 'package:app/src/layout/inicio/InicioWidget.dart';
import 'package:bloc_pattern/bloc_pattern.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

class HomeWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return BlocProvider<HomeBloc>(
        bloc: HomeBloc(context), child: _HomeContainer());
  }
}

class _HomeContainer extends StatefulWidget {
  @override
  __HomeContainerState createState() => __HomeContainerState();
}

class __HomeContainerState extends State<_HomeContainer>
    with SingleTickerProviderStateMixin {
  TabController tabController;
  @override
  void initState() {
    super.initState();
    tabController = TabController(length: 6, vsync: this);
  }

  @override
  void dispose() {
    tabController.dispose();
    super.dispose();
  }


  @override
  Widget build(BuildContext context) {
    return Scaffold(
        endDrawer: Drawer(),
        drawer: DrawerHome(),
        appBar: AppBar(
          title: Padding(
            padding: const EdgeInsets.all(10.0),
            child: Image.asset("assets/imgs/icon.png", fit: BoxFit.contain),
          ),
          actions: [
            Builder(
              builder: (context) => IconButton(
                    icon: Icon(Icons.notifications_none),
                    onPressed: () => Scaffold.of(context).openEndDrawer(),
                    tooltip:
                        MaterialLocalizations.of(context).openAppDrawerTooltip,
                  ),
            ),

            IconButton(onPressed: () {}, icon: Icon(FontAwesomeIcons.signOutAlt),)

          ],
        ),
        bottomNavigationBar: BottomBarWidget(
          secondaryColor: Colors.purple,
          controller: tabController,
          items: <TabItem>[
            TabItem(Icons.home, "início"),
            TabItem(Icons.short_text, "igreja"),
            TabItem(Icons.attach_money, "inscrições"),
            TabItem(Icons.storage, "mídia"),
            TabItem(Icons.center_focus_strong, "células"),
            TabItem(Icons.question_answer, "ajuda"),
          ],
        ),
        body: TabBarView(
          controller: tabController,
          children: [
            InicioWidget(),
            Placeholder(),
            Placeholder(),
            Placeholder(),
            Placeholder(),
            Placeholder(),
          ],
        ));
  }
}
