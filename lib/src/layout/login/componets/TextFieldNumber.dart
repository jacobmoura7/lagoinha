import 'dart:async';

import 'package:app/src/layout/login/LoginBloc.dart';
import 'package:app/src/layout/login/utils/AnimatedButtonsTypes.dart';
import 'package:bloc_pattern/bloc_pattern.dart';
import 'package:flutter/material.dart';
import 'package:flutter_masked_text/flutter_masked_text.dart';

class TextFieldNumber extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final bloc = BlocProvider.of<LoginBloc>(context);
    FocusNode _focus = FocusNode();
    _focus.addListener(() {
      print(_focus.hasFocus);
      if (_focus.hasFocus) {
        Timer(Duration(milliseconds: 300), () {
          bloc.scrollController
              .jumpTo(bloc.scrollController.position.maxScrollExtent);
        });
      }
    });



    return Column(
      children: <Widget>[
        Padding(
          padding: const EdgeInsets.only(bottom: 30, left: 60, right: 60),
          child: StreamBuilder(
              stream: bloc.numberOut,
              initialData: "",
              builder: (context, AsyncSnapshot<String> snapshot) => Theme(
                    data: new ThemeData(
                        primaryColor: Colors.blue,
                        accentColor: Colors.orange,
                        hintColor: Colors.green),
                    child: TextField(
                      focusNode: _focus,
                      scrollPadding: EdgeInsets.all(50),
                      controller: bloc.controllerNumber,
                      onChanged: bloc.numberIn.add,
                      keyboardType: TextInputType.number,
                      style: TextStyle(color: Colors.white),
                      decoration: InputDecoration(
                        hintStyle: TextStyle(color: Colors.white),
                        labelStyle: TextStyle(color: Colors.white),
                        hintText: '+55 (99) 9 9999-9999',
                        labelText: 'Seu número',
                        errorText: snapshot.error,
                      ),
                    ),
                  )),
        ),
        StreamBuilder(
          stream: bloc.numberOut,
          initialData: "",
          builder: (context, AsyncSnapshot<String> snapshot) => RaisedButton(
                color: Colors.green,
                textColor: Colors.white,
                child: Text("Verificar seu número"),
                onPressed: snapshot.error == null ? bloc.loginNumber : null,
              ),
        ),
        Container(
          height: 10,
        ),
        OutlineButton(
          textColor: Colors.white,
          child: Text("Voltar"),
          onPressed: bloc.voltar,
        )
      ],
    );
  }
}
