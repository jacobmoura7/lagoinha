class EstadoCivil{

final int id;
final String nome;

  EstadoCivil(this.id, this.nome);


  static  List<EstadoCivil> allEstadoCivil(){

    return <EstadoCivil>[
      EstadoCivil(0, 'Não especificado'),
      EstadoCivil(1, 'Casado(a)'),
      EstadoCivil(2, 'Solteiro(a)'),
      EstadoCivil(3, 'Viúvo(a)'),
      EstadoCivil(4, 'Divorciado(a)'),
      EstadoCivil(5, 'Separado(a)'),
      EstadoCivil(6, 'Outros'),
      EstadoCivil(7,  'Amaziado(a)'),
      EstadoCivil(8, 'Amigado(a)'),
      EstadoCivil(9, 'União Estavel'),
      EstadoCivil(10, 'Desquitado(a)'),
    ];
                        
  }

}